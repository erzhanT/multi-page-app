import React, {useState} from 'react';


const SecondForm = () => {

    const [items, setItems] = useState([{name: '', price: 0}]);
    const [percent, setPercent] = useState(0);
    const [delivery, setDelivery] = useState(0);
    const [results, setResults] = useState([]);

    const onChangePercent = (e) => {
        setPercent(e.target.value);
    };
    const onChangeDelivery = (e) => {
        setDelivery(e.target.value);
    };

    const addItem = () => {
        setItems([...items, {name: '', price: 0}]);
    };

    const removeItem = name => {
        const itemsCopy = [...items];
        const index = itemsCopy.findIndex(i => i.name === name);
        itemsCopy.splice(index, 1);
        setItems(itemsCopy);
    };


    const onChangeName = (e, i) => {
        const itemsCopy = [...items];
        const itemCopy = {...itemsCopy[i]};
        itemCopy.name = e.target.value;
        itemsCopy[i] = itemCopy;
        setItems(itemsCopy);
    };

    const onChangePrice = (e, i) => {
        const itemsCopy = [...items];
        const itemCopy = {...itemsCopy[i]};
        itemCopy.price = e.target.value;
        itemsCopy[i] = itemCopy;
        setItems(itemsCopy);
    };

    const onButtonHandler = () => {
        const itemsCopy = [...items];

        const newArray = itemsCopy.map(item => {
            const itemCopy = {...item};
            const eachSumPercent = Math.ceil(parseInt(itemCopy.price) * (percent / 100) + (delivery / items.length));
            itemCopy.price = parseInt(itemCopy.price) + eachSumPercent;
            return itemCopy;
        });
        console.log(newArray);
        setResults(newArray);
    };

    const finalResults = results.map((item, i) => (
        <p key={i}><strong>{item.name}</strong>: {item.price} сом</p>
    ));

    return (
        <div>
            {items.map((item, i) => (
                <div key={i}>
                    <input
                        type="text"
                        value={item.name}
                        placeholder="Name"
                        onChange={e => onChangeName(e, i)}
                        required/>&nbsp;
                    <input
                        type="number"
                        value={item.price}
                        placeholder="Price"
                        onChange={e => onChangePrice(e, i)}
                        required/>&nbsp;
                    <button onClick={() => removeItem(item.name)}>-</button>
                </div>
            ))}

            <button onClick={addItem}>+</button>

            <p>Процент чаевых: <input type="number" value={percent} onChange={e => onChangePercent(e)} required/> %</p>
            <p>Доставка: <input type="number" value={delivery} onChange={e => onChangeDelivery(e)}/> сом</p>
            <button onClick={onButtonHandler}>Рассчитать</button>
            <div>
                {finalResults && finalResults}
            </div>
        </div>
    );
};

export default SecondForm;