import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';
import {AppBar, Button, Container, Menu, MenuItem, Toolbar, Typography} from "@material-ui/core";



const Navigation = () => {
    const [anchorEl, setAnchorEl] = useState(false);

    const handleClick = (e) => {
        setAnchorEl(e.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(false);
    };

    return (
        <AppBar>
            <Container fixed>
                <Toolbar>
                    <Button
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        aria-controls="simple-menu"
                        aria-haspopup="true"
                        onClick={handleClick}
                    >
                        Open
                    </Button>
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={anchorEl}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={handleClose}>
                                <NavLink  to={'/'}>Home</NavLink>
                            </MenuItem>
                            <MenuItem onClick={handleClose}>
                                <NavLink to={'/split'}>Split App</NavLink>
                            </MenuItem>
                            <MenuItem onClick={handleClose}>
                                <NavLink to={'/countries'}>Search countries App</NavLink>
                            </MenuItem>
                        </Menu>


                    <Typography variant="h5">My Home Page</Typography>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default Navigation;