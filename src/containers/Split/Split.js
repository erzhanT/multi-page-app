import React, {useState} from 'react';
import SecondForm from "../../components/SecondForm/SecondForm";
import FirstForm from "../../components/FirstForm/FirstForm";
import './Split.css'

const Split = () => {

    const [mode, setMode] = useState('first');

    const onRadioChange = e => {
        setMode(e.target.value);
    }

    return (
        <div className="Split-app">
            <p>
                <input
                    type="radio"
                    name="options"
                    value="first"
                    onChange={onRadioChange}
                    checked={mode === 'first'}
                /> Поровну между всеми участниками
            </p>
            <p>
                <input
                    type="radio"
                    name="options"
                    value="second"
                    onChange={onRadioChange}
                    checked={mode === 'second'}
                /> Каждому индивидуально
            </p>
            <div>
                {mode === 'first' && (
                    <FirstForm/>
                )}
                {mode === 'second' && (
                    <SecondForm/>
                )}
            </div>
        </div>
    );
};

export default Split;