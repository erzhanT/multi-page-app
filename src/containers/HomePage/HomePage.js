import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {List, ListItem, Paper, Typography} from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import Navigation from "../../components/Navigation/Navigation";
import Avatar from "../../components/Image/Avatar";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    typography: {
        padding: theme.spacing(2),
        textAlign: 'center'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center'
    }

}));


const HomePage = () => {

    const classes = useStyles();

    return (
        <>
            <Navigation/>
            <div
                className={classes.root}
                style={{marginTop: '100px'}}
            >
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Typography className={classes.typography} variant="h3">Турдумамбетов Эржан</Typography>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Avatar/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <List>
                            <ListItem>
                                <b>Дата рождения: </b> &nbsp; <span>01.07.1998 г.</span>
                            </ListItem>
                        </List>
                        <List>
                            <ListItem>
                                <b>Адрес:</b> &nbsp; <span>г. Бишкек, 7-микрорайон дом 5-б квартира 13</span>
                            </ListItem>
                        </List>
                        <List>
                            <ListItem>
                                <b>Электронная почта:</b> &nbsp; <span>turdumambetov.erzhan@gmail.com</span>
                            </ListItem>
                        </List>
                        <List>
                            <ListItem>
                                <b>Телефон:</b> &nbsp; <span>+996 700 010 798</span>
                            </ListItem>
                        </List>
                        <List>
                            <ListItem>
                                <b>Гражданство:</b> &nbsp; <span>Кыргызская Республика</span>
                            </ListItem>
                        </List>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='h5' style={{margin: '20px 30px'}}>ОБРАЗОВАНИЕ</Typography>
                        <hr/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Paper className={classes.paper}>2020-2021</Paper>
                        <Paper className={classes.paper}>2017-2021</Paper>
                        <Paper className={classes.paper}>2005-2016</Paper>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Paper className={classes.paper}>Attractor school. Язык программирования: JavaScript</Paper>
                        <Paper className={classes.paper}>КРСУ, ФМО. Направление: Политология</Paper>
                        <Paper className={classes.paper}>УВКШГ №67 г.Бишкек</Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='h5' style={{margin: '20px 30px'}}>ТЕХНИЧЕСКИЕ НАВЫКИ</Typography>
                        <hr/>
                    </Grid>
                    <Grid item xs={12} style={{margin: '0 30px'}}>
                        <Paper spacing='3'> В процессе обучения в школе программирования «Attractor School»
                            занимался разработкой многочисленных проектов. В ходе обучения мною были
                            освоены и получены следующие технические навыки:</Paper>
                        <Paper className={classes.paper}>• Технологии и Фреймворки: HTML/CSS, Vanilla JS, jQuery,
                            React.js (базовые знания)</Paper>
                        <Paper className={classes.paper}>• IDE: WebStorm.</Paper>
                        <Paper className={classes.paper}>• Building tools: Bitbucket, GitHub.</Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='h5' style={{margin: '20px 30px'}}>ОБЩЕСТВЕННАЯ РАБОТА</Typography>
                        <hr/>
                    </Grid>
                    <Grid item xs={12} style={{margin: '20px 30px'}}>
                        <Paper className={classes.paper}>• Организация учебного судебного процесса по правам человека
                            (2019 г.)</Paper>
                        <Paper className={classes.paper}>• Участие в организации Саммита глав государств ШОС в г.Бишкек
                            (2019 г.)</Paper>
                        <Paper className={classes.paper}>• Участие в организации Совета коллективной безопасности ОДКБ в
                            г.Бишкек (2019 г.)</Paper>
                        <Paper className={classes.paper}>• Организация университетских деловых игр в КРСУ (2018
                            г.)</Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='h5' style={{margin: '20px 30px'}}>ЗНАНИЕ ЯЗЫКОВ И ПРОЧИЕ
                            НАВЫКИ</Typography>
                        <hr/>
                    </Grid>
                    <Grid item xs={12} style={{margin: '20px 30px'}}>

                        <Paper className={classes.paper}>Кыргызский язык (свободно)</Paper>
                        <Paper className={classes.paper}>Русский язык (свободно) </Paper>
                        <Paper className={classes.paper}>Английский язык (Intermediate)</Paper>
                        <Paper className={classes.paper}>Испанский язык (со словарем)</Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='h5' style={{margin: '20px 30px'}}>СВЕДЕНИЯ О СЕБЕ</Typography>
                        <hr/>
                    </Grid>
                    <Grid item xs={12} style={{margin: '20px 30px'}}>
                        <Paper className={classes.paper}>Умение быстро обучаться, работать в команде,
                            нестандартное математическое мышление, коммуникабельность,
                            ответственность, исполнительность, пунктуальность.</Paper>
                        <Paper className={classes.paper}>Семейное положение: холост</Paper>
                        <Paper className={classes.paper}>Водительские права категории: B1</Paper>
                    </Grid>

                </Grid>

            </div>

        </>
    );
};

export default HomePage;