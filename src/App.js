import React from "react";
import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import Countries from "./containers/Countries/Countries";
import Navigation from "./components/Navigation/Navigation";
import Split from './containers/Split/Split'

const App = () => {



    return (
        <BrowserRouter>
            <Navigation />
            <Switch>
                <Route path="/" exact component={HomePage}/>
                <Route path="/countries" component={Countries}/>
                <Route path="/split" component={Split}/>
            </Switch>

            {/*<Countries/>*/}
        </BrowserRouter>
    );
};

export default App;
